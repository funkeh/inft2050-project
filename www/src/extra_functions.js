// WNJIndustries :: 2016
// Extra functions for use in this project

// Extends String for time conversion to MM:SS
String.prototype.toMMSS = function () {

    var sec_num = parseInt(this, 10);
    var minutes = Math.floor((sec_num) / 60);
    var seconds = sec_num - (minutes * 60);

    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return minutes + ':' + seconds;
};


// Plays a sound at game's volume
// INPUT: Sound to be played
function playSFX(sound) {
	sound.play('', 0, game.volume);
}


// Find all objects from a tilemap layer
// INPUT: map []
function findObjects(map, layer) {
	var result = new Array();
	map.objects[layer].forEach(function(element) {
		//Phaser uses top left, Tiled bottom left so we have to adjust
		//also keep in mind that the cup images are a bit smaller than the tile which is 16x16
		//so they might not be placed in the exact position as in Tiled
		element.y -= map.tileHeight;
		result.push(element);
	});
	return result;
}

// Create a sprite from an object
function createFromTiledObject(element, group) {

	var sprite = group.create(element.x, element.y, element.properties.sprite);

	//copy all properties to the sprite
	Object.keys(element.properties).forEach(function(key){
		sprite[key] = element.properties[key];
	});
}
