/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the level select

LevelSelectState = {

    init: function () { },

    preload: function () { },

    create: function () {

        var BUTTON_X = this.world.centerX - 372; // X position of buttons

        // Create background and title
        this.bg = this.add.sprite(0, 0, 'bg');
        this.head = this.add.sprite(this.world.centerX - 285, 96, 'lvl_head');

		// Back button
		this.back = this.add.button(40, 40, 'back', this.goBack);

		// Record current position of indicator and create indicator
		this.key = game.level;
		this.indicator = this.add.sprite(BUTTON_X - 4, 220 + (this.key - 1) *
		                                 96, 'lvl_light');

		// Create screenshot box
		this.preview = this.add.sprite(BUTTON_X + 96, 224, 'lvl_preview');
		this.screenshot = this.add.sprite(BUTTON_X + 100, 228, 'lvl_scr' +
		                                  this.key);

        // Add buttons with functions
        this.button = {
            lvl1: this.add.button(BUTTON_X, 224, 'lvl1', this.selectLevel),
            lvl2: this.add.button(BUTTON_X, 320, 'lvl2', this.selectLevel),
            lvl3: this.add.button(BUTTON_X, 416, 'lvl3', this.selectLevel),
            lvl4: this.add.button(BUTTON_X, 512, 'lvl4', this.selectLevel),
			play: this.add.button(BUTTON_X + 100, 228, 'lvl_play',
			                      this.startLevel, this)
        };
    },

	update: function () { },

	goBack: function() {

		// Return to main menu
		playSFX(game.sfx.click);
		game.state.start('menu');
	},

    // Change state based on click functions
    selectLevel: function() {

		// Determine state variable
		var state = game.state.states.level_select;

		// Determine level number
		state.key = Number(this.key.substr(3));

		// Change level selection indicator position
		state.indicator.y = 220 + (state.key - 1) * 96;

		state.screenshot.loadTexture("lvl_scr" + state.key);

		playSFX(game.sfx.click);
	},

	// Start level
	startLevel: function() {

		playSFX(game.sfx.click);
		game.level = this.key;
		game.state.start('play');
	}
};
