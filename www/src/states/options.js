/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the options

OptionsState = {

    init: function () { },

    preload: function () { },

    create: function () {

        this.vol_sliding = false;

        var BUTTON_X = this.world.centerX - 372; // X position of buttons

        // Create background and title
        this.bg = this.add.sprite(0, 0, 'bg');
        this.head = this.add.sprite(this.world.centerX - 285, 96, 'opt_head');

		// Back button
		this.back = this.add.button(40, 40, 'back', this.goBack);

		this.vol = this.add.sprite(575, 384, 'opt_volume');

		// Record current position of indicator and create indicator
		this.slider_bar = this.add.sprite(240, 428, 'opt_slider_bar');

        // Add buttons with functions
        this.slider = this.add.button(1040, 448, 'opt_slider', this.sFinish,
                                      this);
        this.slider.anchor.setTo(0.5, 0.5);

		this.slider.onInputDown.add(this.sStart, this);
    },

	update: function () {
        if (this.vol_sliding) {
            this.slider.x = game.input.activePointer.x;
            this.slider.x = Math.max(Math.min(this.slider.x, 1040), 240);
			game.volume = (this.slider.x - 240) / 800;
        }
    },

	goBack: function() {

		// Return to main menu
		playSFX(game.sfx.click);
		game.state.start('menu');
	},

    // Change state based on click functions
    sStart: function() {

		// Determine state variable
		playSFX(game.sfx.click);
		this.vol_sliding = true;
	},

	// Start level
	sFinish: function() {

		// Determine state variable
		this.vol_sliding = false;
	}
};
