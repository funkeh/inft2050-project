/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the main menu

MenuState = {

    init: function () { },

    preload: function () { },

    create: function () {

        var BUTTON_X = this.world.centerX - 210;

        // Create background and logo
        this.bg = this.add.sprite(0, 0, 'bg');
        this.logo = this.add.sprite(this.world.centerX - 285, 96, 'logo');

        // Add buttons with functions
        this.button = {
            start: this.add.button(BUTTON_X, 224, 'b_start', this.startCl),
            options: this.add.button(BUTTON_X, 320, 'b_options', this.optCl),
            scores: this.add.button(BUTTON_X, 416, 'b_scores', this.scoresCl),
            credits: this.add.button(BUTTON_X, 512, 'b_credits', this.creditsCl)
        };
    },

	update: function () { },

    // Change state based on click functions
    startCl:   function() {
		playSFX(game.sfx.click);
		game.state.start('level_select');
	},
    optCl:     function() {
		playSFX(game.sfx.click);
		game.state.start('options');
	},
    scoresCl:  function() {
		playSFX(game.sfx.click);
		game.state.start('scores');
	},
    creditsCl: function() {
		playSFX(game.sfx.click);
		game.state.start('credits');
	}
};
