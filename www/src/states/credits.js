/* globals Phaser:false */
// WNJIndustries :: 2016
// State for the credits

CreditsState = {

    init: function () { },

    preload: function () { },

    create: function () {

        // Create background and title
        this.bg = this.add.sprite(0, 0, 'bg');
        this.head = this.add.sprite(this.world.centerX - 285, 96, 'cdt_head');

		this.credits = this.add.sprite(64, 168, 'cdt_body');

		// Back button
		this.back = this.add.button(40, 40, 'back', this.goBack);
    },

	update: function () { },

	goBack: function() {

		// Return to main menu
		playSFX(game.sfx.click);
		game.state.start('menu');
	}
};
